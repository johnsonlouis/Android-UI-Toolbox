package com.accorhotels.rami.appthemes.fragments;

import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;

import com.accorhotels.rami.appthemes.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by rhamrouni on 26/03/2018.
 */

public class TabFragment extends BaseFragment {

    private ViewPager viewPager;

    public int getResourceId(){
        return R.layout.fragment_tabs;
    }

    @Override
    public void initUI(){
        setUpTabs();
    }

    private void setUpTabs(){
        viewPager = getView().findViewById(R.id.viewpager);
        setupViewPager();

        TabLayout tabLayout = getView().findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
    }

    private void setupViewPager() {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getActivity().getSupportFragmentManager());
        adapter.addFrag(new TabItemFragment(), "ONE");
        adapter.addFrag(new TabItemFragment(), "TWO");
        adapter.addFrag(new TabItemFragment(), "THREE");
        adapter.addFrag(new TabItemFragment(), "FOUR");
        adapter.addFrag(new TabItemFragment(), "FIVE");
        adapter.addFrag(new TabItemFragment(), "SIX");
        viewPager.setAdapter(adapter);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFrag(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }
}
