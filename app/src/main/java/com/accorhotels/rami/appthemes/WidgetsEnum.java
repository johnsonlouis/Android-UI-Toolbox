package com.accorhotels.rami.appthemes;

/**
 * Created by rhamrouni on 23/03/2018.
 */

public enum WidgetsEnum {

    WIDGETS(1, "Widgets"), TEXTS(2, "Texts"), TABS(3, "Tabs");

    int value;
    String title;

    WidgetsEnum(int value, String title) {
        this.value = value;
        this.title = title;
    }

    static WidgetsEnum fromValue(int id) {
        for (WidgetsEnum f : values()) {
            if (f.value == id) return f;
        }
        return WidgetsEnum.fromValue(1);
    }

    public int getValue() {
        return value;
    }

    public String getTitle() {
        return title;
    }
}
