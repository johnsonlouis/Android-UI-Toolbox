package com.accorhotels.rami.appthemes.fragments;

import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.accorhotels.rami.appthemes.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by rhamrouni on 26/03/2018.
 */

public class WidgetsFragment extends BaseFragment {

    public int getResourceId(){
        return R.layout.fragment_widgets;
    }

    @Override
    public void initUI(){
        setUpSpinner();
    }

    private void setUpSpinner(){
        Spinner spinner = getView().findViewById(R.id.spinner);
        List<String> list = new ArrayList<>();
        list.add("spinner item 1");
        list.add("spinner item 2");
        list.add("spinner item 3");
        list.add("spinner item 4");
        list.add("spinner item 5");
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item, list);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(dataAdapter);
    }
}
