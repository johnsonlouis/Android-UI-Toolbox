package com.accorhotels.rami.appthemes.fragments;

import com.accorhotels.rami.appthemes.R;

/**
 * Created by rhamrouni on 26/03/2018.
 */

public class TabItemFragment extends BaseFragment {

    public int getResourceId(){
        return R.layout.fragment_tab_item;
    }
}
