package com.accorhotels.rami.appthemes.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


/**
 * Created by rhamrouni on 23/03/2018.
 */

public class BaseFragment extends Fragment {

    private View view;

    public View onCreateView(LayoutInflater inflater, ViewGroup vg, Bundle savedInstanceState) {
        view = inflater.inflate(getResourceId(), vg, false);
        initUI();
        return view;
    }

    public int getResourceId(){
        return 0;
    }

    public void initUI() {
        // do nothing
    }

    public View getView(){
        return view;
    }
}
