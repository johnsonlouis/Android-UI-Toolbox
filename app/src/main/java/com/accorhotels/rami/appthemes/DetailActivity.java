package com.accorhotels.rami.appthemes;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.accorhotels.rami.appthemes.constants.Const;
import com.accorhotels.rami.appthemes.fragments.TabFragment;
import com.accorhotels.rami.appthemes.fragments.TextFragment;
import com.accorhotels.rami.appthemes.fragments.WidgetsFragment;

/**
 * Created by rhamrouni on 23/03/2018.
 */

public class DetailActivity extends AppCompatActivity {

    private String pageTitle;
    private WidgetsEnum widgetType;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        Intent intent = getIntent();
        pageTitle = intent.getStringExtra(Const.KEY_PAGE_TITLE);
        widgetType = WidgetsEnum.fromValue(intent.getIntExtra(Const.KEY_WIDGET_TYPE, 1));

        setupTollbar();
        setupFragment();
    }

    private void setupTollbar(){
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle(pageTitle);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    private void setupFragment(){
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        fragmentTransaction.replace(R.id.container, getFragment());
        fragmentTransaction.commit();

    }

    private Fragment getFragment(){
        switch (widgetType){
            case TEXTS:
                return new TextFragment();
            case WIDGETS:
                return new WidgetsFragment();
            case TABS:
                return new TabFragment();
            default:
        }
        return new Fragment();
    }
}
