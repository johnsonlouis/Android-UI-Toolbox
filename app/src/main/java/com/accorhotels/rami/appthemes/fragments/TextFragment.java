package com.accorhotels.rami.appthemes.fragments;

import com.accorhotels.rami.appthemes.R;

/**
 * Created by rhamrouni on 23/03/2018.
 */

public class TextFragment extends BaseFragment {

    public int getResourceId(){
        return R.layout.fragment_text;
    }
}
