package com.accorhotels.rami;

import com.accorhotels.rami.appthemes.WidgetsEnum;

/**
 * Created by ddiai on 26/03/2018.
 */

public interface OnItemClickListener {
    void onItemClick(WidgetsEnum widgetsEnum);
}
