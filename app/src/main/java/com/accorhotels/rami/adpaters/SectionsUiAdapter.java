package com.accorhotels.rami.adpaters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.accorhotels.rami.OnItemClickListener;
import com.accorhotels.rami.appthemes.R;
import com.accorhotels.rami.appthemes.WidgetsEnum;

import java.util.List;

/**
 * Created by ddiai on 26/03/2018.
 */

public class SectionsUiAdapter extends RecyclerView.Adapter<SectionsUiAdapter.ViewHolder> {

    private Context context;
    private List<WidgetsEnum> sectionsList;
    private OnItemClickListener listener;

    public SectionsUiAdapter(Context context, List<WidgetsEnum> sectionsList, OnItemClickListener listener) {
        this.context = context;
        this.sectionsList = sectionsList;
        this.listener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_section_ui, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        final WidgetsEnum widgetsEnum = sectionsList.get(position);
        holder.tvTitle.setText(widgetsEnum.getTitle());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemClick(widgetsEnum);
            }
        });
    }

    @Override
    public int getItemCount() {
        return sectionsList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder
    {
        TextView tvTitle;

        ViewHolder(View itemView) {
            super(itemView);
            tvTitle = itemView.findViewById(R.id.tv_title);
        }
    }
}
